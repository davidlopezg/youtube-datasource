""" This module is a simple test for the YouTube transform function """
from youtube_insights_transfom import process

DATA = '[{"day":"2017-04-05","views":1,"estimatedMinutesWatched":0,"averageViewDuration":9,"averageViewPercentage":2.164425162689805,"subscribersGained":0},{"day":"2017-04-06","views":1,"estimatedMinutesWatched":0,"averageViewDuration":4,"averageViewPercentage":0.9409978308026031,"subscribersGained":0},{"day":"2017-09-04","views":1,"estimatedMinutesWatched":0,"averageViewDuration":15,"averageViewPercentage":3.431887201735358,"subscribersGained":0}]'
CONFIG = {}
TRANSFORMRESULT = process(DATA, CONFIG)

print(TRANSFORMRESULT)
