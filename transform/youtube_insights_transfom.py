"""This module is the main transform function for YouTube's API """

def process(data, config):
  """
  This method do the transformation process for the YouTube API response
  """

  print('Init youtube transform function')
  print(config)
  import json

  json_data = json.loads(data)
  return json_data
