import * as fs from 'fs';
import * as readline from 'readline';
import {google} from 'googleapis';
import {YoutubeParams} from './Models/params';

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/youtube-nodejs-quickstart.json
const OAuth2 = google.auth.OAuth2;
const SCOPES = ['https://www.googleapis.com/auth/youtube.readonly'];
const TOKEN_DIR = './assets/';
const TOKEN_PATH = TOKEN_DIR + 'youtube-nodejs-quickstart.json';

export class YouTubeHelper {
  /**
   * Constructor
   */
  constructor() {}

  /**
   * Store token to disk be used in later program executions.
   *
   * @param {Object} token The token to store to disk.
   */
  storeToken(token: string) {
    try {
      fs.mkdirSync(TOKEN_DIR);
    } catch (err) {
      if (err.code != 'EXIST')
        throw err;
    }
    fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err: any) => {
      if (err) throw err;
      console.log('Token stored to ' + TOKEN_PATH);
    });
    console.log('Token stored to ' + TOKEN_PATH);
  }

  /**
     * Get and store new token after prompting for user authorization, and then
     * execute the given callback with the authorized OAuth2 client.
     *
     * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
     * @param {getEventsCallback} callback The callback to call with the authorized
     *     client.
     */
  getNewToken(oauth2Client: any, callback: any) {
    const authUrl = oauth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url: ', authUrl);
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code:string) => {
      rl.close();
      oauth2Client.getToken(code, (err:any, token: any) => {
        if (err) {
          console.log('Error while trying to retrieve access token', err);
          return;
        }
        oauth2Client.credentials = token;
        this.storeToken(token);
        callback(oauth2Client);
      });
    });
  }

  /**
   * Method used for check token and call the YouTube API
   * @param {event} event parameters to process the event
   * @return {Promise} Returns a promise
   */
  processEvent(event: YoutubeParams) {
    return new Promise((resolve, reject) => {
      const credentials = event.credentials;
      const clientSecret = credentials.client_secret;
      const clientId = credentials.client_id;
      const redirectUrl = credentials.redirect_uris[0];
      const oauth2Client = new OAuth2(clientId, clientSecret, redirectUrl);

      // Check if we have previously stored a token.
      fs.readFile(TOKEN_PATH, (err: any, token: any) => {
        if (err) {
          this.getNewToken(oauth2Client, this.getReport);
        } else {
          oauth2Client.credentials = JSON.parse(token);
          this.getReport(event, oauth2Client, resolve);
        }
      });
    });
  };

  /**
   * Lists the names and IDs of up to 10 files.
   * @param {YoutubeParams} event Event object.
   * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
   * @param {any} resolve Function resolve
   */
  getReport(event: YoutubeParams, auth: any, resolve:any) {
    const youtubeAnalytics = google.youtubeAnalytics('v2');
    
    youtubeAnalytics.reports
      .query({
        ids: event.channells.toString(), // "channel==MINE",
        startDate: event.start_date,
        endDate: event.end_date,
        metrics: event.metrics.toString(),
        dimensions: event.dimensions.toString(), // "day",
        sort: 'day',
        auth,
      })

      .then((results: any) => {
        console.log('status : ' + JSON.stringify(results.status));
        
        // Sanitize
        const headers = results.data.columnHeaders;
        const rows = results.data.rows;
        
        const list: object[] = [];
        rows.forEach((result: any) => {
          const object = headers.reduce((x:any, y:any) => {
            x[y.name] = '';
            return x;
          }, {});
          result.forEach((row: any, index: number) => {
            object[headers[index].name] = row;
          });
          list.push(object);
        });
        resolve(list);
      })
      .catch((error:string) => {
        console.log(error); // error is cought here
      });
  };
}
