import {YouTubeHelper} from './youtube_helper';
import {YoutubeParams} from './Models/params';
import * as payloadHelper from './payload_helper';

/**
* Main function for lambda instace
* @param {YoutubeParams} event Evento
* @return {Promise} Retorna una promesa
*/
export function handler(event: YoutubeParams) {
  console.log('Init Lambda execution!!!');
  const youtubeHelper: YouTubeHelper = new YouTubeHelper();
  const config = JSON.parse(event.config);

  const eventConf : any = {
    ds_name: event.ds_name,
    current_step: 'init',
    config: config,
    run_id: event.run_id,
    data: '',
  };

  return new Promise((resolve, reject) => {
    youtubeHelper.processEvent(event)
      .then((result) => {
        console.log('processEvent OK : ', JSON.stringify(result));

        eventConf.data = JSON.stringify([{report: result}]);
        payloadHelper.sendPayload(eventConf, function(error) {
          if (error) reject(error);
          else resolve(result);
        });
      })
      .catch((error) => {
        console.log('processEvent ERROR : ', JSON.stringify(error));
        reject(error);
      });
  });
};
